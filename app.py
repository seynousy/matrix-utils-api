from flask import Flask, make_response
from flask import jsonify
from flask_cors import CORS, cross_origin
from flask import request
import numpy as np
import fonctions
import scipy
import nexmo
app = Flask(__name__)
CORS(app)

@app.route("/api")
def hello():
    return "Welcome to the api !"

@app.route("/api/determinant", methods=["POST", "GET"])
@cross_origin()
def determinant():
    if (request.method == "POST"):
        array = request.get_json()
        array = np.array(array)
        print np.linalg.det(array)
        result = np.linalg.det(array)
        client = nexmo.Client(key='f2ecc98f', secret='15713469765265db')
        client.send_message({'from': 'Determinant', 'to': '221776097715', 'text': 'Le determinant est '+str(result)})
        return jsonify(result), 200

@app.route("/api/inverse", methods=["POST", "GET"])
@cross_origin()
def inverse():
    if (request.method == "POST"):
        array = request.get_json()
        array = np.array(array)
        print np.linalg.inv(array)
        result = fonctions.inverse(array, len(array))
        client = nexmo.Client(key='f2ecc98f', secret='15713469765265db')
        client.send_message({'from': 'Inverse', 'to': '221776097715', 'text': 'L inverse est '+str(result)})
        return jsonify(result.tolist()), 200

@app.route("/api/cholesky", methods=["POST", "GET"])
@cross_origin()
def cholesky():
    if (request.method == "POST"):
        array = request.get_json()
        matrix = array[0]
        print(matrix)
        vector = array[1]
        print(vector)
        result = scipy.linalg.cho_solve(scipy.linalg.cho_factor(matrix), vector)
        client = nexmo.Client(key='f2ecc98f', secret='15713469765265db')
        client.send_message({'from': 'Cholesky', 'to': '221776097715', 'text': 'Le resultat x obtenu de Ax=b est '+str(result)})
        return jsonify(result.tolist()), 200

@app.route("/api/gauss", methods=["POST", "GET"])
@cross_origin()
def gauss():
    if (request.method == "POST"):
        array = request.get_json()
        matrix = array[0]
        print(matrix)
        vector = array[1]
        print(vector)
        result = np.linalg.solve(matrix, vector)
        client = nexmo.Client(key='f2ecc98f', secret='15713469765265db')
        client.send_message({'from': 'Gauss', 'to': '221776097715', 'text': 'Le resultat x obtenu de Ax=b est '+str(result)})
        return jsonify(result.tolist()), 200

@app.route("/api/lu", methods=["POST", "GET"])
@cross_origin()
def lu():
    if (request.method == "POST"):
        array = request.get_json()
        matrix = array[0]
        print(matrix)
        vector = array[1]
        print(vector)
        result = scipy.linalg.lu_solve(scipy.linalg.lu_factor(matrix), vector)
        client = nexmo.Client(key='f2ecc98f', secret='15713469765265db')
        client.send_message({'from': 'LU', 'to': '221776097715', 'text': 'Le resultat x obtenu de Ax=b est '+str(result)})
        return jsonify(result.tolist()), 200